﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{

    public float speed;
    public Rigidbody2D myRigidbody;
    void Update()
    {
        float inputX = Input.GetAxis("Horizontal") * speed;
        myRigidbody.AddForce(Vector2.right * inputX);

    }


    public void OnCollisionStay2D()
    {
        if (Input.GetButtonDown("Jump"))
        {
            myRigidbody.AddForce(Vector2.up * 10, ForceMode2D.Impulse);
        }
    }
}