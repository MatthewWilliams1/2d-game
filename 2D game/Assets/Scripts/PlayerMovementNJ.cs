﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovementNJ : MonoBehaviour {


    public float speed;
    public Rigidbody2D myRigidbody;
    void Update()
    {
        float inputX = Input.GetAxis("Horizontal") * speed;
        myRigidbody.AddForce(Vector2.right * inputX);

    }
}
